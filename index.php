<?php
    require 'medoo.php';

    // for users
    $src = new medoo([
    	'database_type' => 'mysql',     # driver name (for MySQL - mysql)
    	'database_name' => 'mydb',      # database name
    	'server' => 'mysql1',           # hostname (for example "localhost")
        'port' => 3306,                 # port (default 3306, so you can delete this line, that's just example)
    	'username' => 'root',           # user login
    	'password' => 'password',       # user password
    	'charset' => 'utf8',            # connection charset
    	'option' => [PDO::ATTR_CASE => PDO::CASE_NATURAL]
    ]);

    // for app_users
    // P.S. I recommend you to use UNIQUE INDEX for "email" column to be sure that all is okay during process of transferring
    $dst = new medoo([
    	'database_type' => 'mysql',     # driver name (for MySQL - mysql)
    	'database_name' => 'mydb',      # database name
    	'server' => 'mysql2',           # hostname (for example "localhost")
        'port' => 3306,                 # port (default 3306, so you can delete this line, that's just example)
    	'username' => 'root',           # user login
    	'password' => 'password',       # user password
    	'charset' => 'utf8',            # connection charset
    	'option' => [PDO::ATTR_CASE => PDO::CASE_NATURAL]
    ]);

    // IMPORTANT NOTE!
    // DON'T FORGET TO CHANGE THIS QUERY (interval and so on)

    $srcData = $src->query("select `email`, `password_hash` as `password`, `created_at` as `registerDate` from `users` where `created_at` > DATE_SUB(NOW(), INTERVAL 1 HOUR)")->fetchAll(PDO::FETCH_ASSOC);
    if(count($srcData) <= 0) {
        die('No data to copy');
    }
    var_dump($srcData);

    // Insertion

    $ids = $dst->insert('app_users', $srcData);

    if($err = $dst->error()) {
        echo "Error: ".var_export($dst->error(), true)."<br><br>";
    }

    // Update ids

    $preparedIds = [];
    foreach($ids as $id) {
      $preparedIds[] = ['user_id' => $id, 'group_id' => 2];
    }

    $dst->insert('app_user_usergroup_map', $preparedIds);

    if($err = $dst->error()) {
        echo "Error: ".var_export($dst->error(), true)."<br><br>";
    }
